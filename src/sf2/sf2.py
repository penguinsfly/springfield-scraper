"""Springfield! Springfield! script scraper"""

import asyncio
import copy
import os
import pkgutil
import re
import warnings
from typing import List, Optional, Union

import httpx
import nest_asyncio  # type: ignore
import pandas as pd
import requests
import rich
import rich.progress
import yaml
from bs4 import BeautifulSoup, NavigableString, Tag

nest_asyncio.apply()

def_cfg = yaml.safe_load(pkgutil.get_data(__package__, "default_config.yaml"))  # type: ignore [arg-type]
httpx_def_cfg = def_cfg["httpx"]

MAIN_URL = "https://www.springfieldspringfield.co.uk"


def request(url: str, soup: bool = True) -> Union[str, bytes, BeautifulSoup]:
    """
    Request content from URL using `requests` package

    Parameters
    ----------
    url: str
        The URL to request from
    soup: bool, default: True
        Whether to return the content (False) or `BeautifulSoup` (True)

    Returns
    -------
    str, bytes or BeautifulSoup
        The content or `BeautifulSoup` of such content

    """

    request = requests.get(url)
    assert request.status_code == 200, f'Error at requesting "{url}"'
    out = request.content
    if soup:
        return BeautifulSoup(out, "lxml")
    return out


def __txt_soupfd__(s: Union[Tag, NavigableString, None]) -> str:
    """Extract text from `soup.find` output. Throw error if `None` is given"""
    assert s is not None
    assert hasattr(s, "text")
    return s.text.strip()


def process_metadata(soup: BeautifulSoup, is_movie: bool = False) -> dict:
    """
    Process metadata from a script page `BeautifulSoup`

    Parameters
    ----------
    soup: BeautifulSoup
        The `BeautifulSoup` of a page content
    is_movie: bool, default: False
        Whether this is a page of a movie or a TV show episode

    Returns
    -------
    dict
        A dictionary containing:
        `id_text`: str
            The `h1` title of the page
        `movie`: str
            Present only if `is_movie = True`
            The title of the movie as inferred from `h1`
        `show`: str
        `season`: int
        `episode`: int
            Present only if `is_movie = False`
            The show title, season number and episode number from both `h1`
        `episode_title`: str
            Present only if `is_movie = False`
            Episode title inferred from `h3`

    """

    ID = __txt_soupfd__(soup.find("h1"))

    assert ID is not None
    metadata = dict(id_text=ID)

    if is_movie:
        assert "movie script" in ID.lower()
        metadata.update(
            dict(
                movie=re.compile(r"\s+(movie\s+script)", re.IGNORECASE)
                .sub("", ID)
                .strip()
            )
        )

    else:
        assert "episode script" in ID.lower()
        regex_pattern = r"(?P<show>.+)\s+s(?P<season>\d+)\s*e(?P<episode>\d+)"
        ep_meta_search = re.search(regex_pattern, ID, re.IGNORECASE)
        assert ep_meta_search is not None
        ep_meta = ep_meta_search.groupdict()  # type: ignore [assignment]
        ep_meta["season"] = (
            None if (_s := ep_meta.get("season", None)) is None else int(_s)
        )
        ep_meta["episode"] = (
            None if (_e := ep_meta.get("episode", None)) is None else int(_e)
        )
        metadata.update(dict(**ep_meta, episode_title=__txt_soupfd__(soup.find("h3"))))

    return metadata


async def process_request(
    client: httpx.AsyncClient,
    url: str,
    progress: Optional[rich.progress.Progress] = None,
    task: Optional[rich.progress.TaskID] = None,
) -> dict:
    """
    Request processor for each URL from async client

    Parameters
    ----------
    client: httpx.AsyncClient
        HTTPX Async client
    url: str
        The URL to request from
    progress: rich.progress.Progress, default: None
        Progress bar object
    task: rich.progress.TaskID
        The task to update progress from, advance by 1 for each request

    Returns
    -------
    dict
        Result of the processed request. See `process_metadata`.
        `id_text`: str
            The `h1` title of the page
        `movie`: str
            Present only if this is a movie page
            The title of the movie as inferred from `h1`
        `show`: str
        `season`: int
        `episode`: int
            Present only if this is an episode page
            The show title, season number and episode number from both `h1`
        `episode_title`: str
            Present only if this is an episode page
            Episode title inferred from `h3`
        `script`: str
            The transcript of the TV episode or movie
        `url`: str
            The requested URL

    """

    response = await client.get(url)  # type: ignore [misc]
    soup = BeautifulSoup(response.content, "lxml")

    is_movie = "movie_script.php?" in url
    is_episode = "view_episode_scripts.php?" in url

    assert is_movie + is_episode == 1, (
        f"Something wrong with {url}. "
        'Needs to contain only "movie_script.php" or "view_episode_scripts.php?"'
    )

    metadata = process_metadata(soup, is_movie=is_movie)

    script_soup = soup.find(class_="scrolling-script-container")
    assert script_soup is not None
    script = script_soup.get_text(strip=True, separator="\n")

    out = dict(**metadata, script=script, url=url)

    if progress is not None:
        assert task is not None
        progress.update(task, advance=1)

    return out


async def scrape_scripts(urls: List[str], httpx_cfg: dict = dict()) -> pd.DataFrame:
    """
    Scrape scripts from a list of URLs

    Parameters
    ----------
    urls: List[str]
        List of URLs
    httpx_cfg: dict, default: dict()
        Configuration for HTTPX. If any of the following is empty, will use `sf2.httpx_def_cfg`.
        Please refer to `httpx` documentation for the following:
        - `retries`
        - `max_keepalive_connections`
        - `max_connections`
        - `keepalive_expiry`
        - `timeout`

    Returns
    -------
    pd.DataFrame
        A pandas DataFrame with the results from scraping

    """

    final_httpx_cfg = copy.deepcopy(httpx_def_cfg)
    final_httpx_cfg.update(**httpx_cfg)

    with rich.progress.Progress(
        rich.progress.TextColumn(
            "[green]{task.description}[/green]", table_column=rich.table.Column(ratio=1)
        ),
        "[white][progress.percentage]{task.percentage:>3.0f}%[/white]",
        rich.progress.BarColumn(
            bar_width=None, table_column=rich.table.Column(ratio=2)
        ),
        rich.progress.TextColumn(
            "[green][{task.completed}/{task.total}][/green]",
            table_column=rich.table.Column(ratio=1),
        ),
        rich.progress.SpinnerColumn(table_column=rich.table.Column(ratio=1)),
        transient=True,
    ) as progress:
        task = progress.add_task("Requesting ...", total=len(urls))

        async with httpx.AsyncClient(
            transport=httpx.AsyncHTTPTransport(retries=final_httpx_cfg["retries"]),
            limits=httpx.Limits(
                max_keepalive_connections=final_httpx_cfg["max_keepalive_connections"],
                max_connections=final_httpx_cfg["max_connections"],
                keepalive_expiry=final_httpx_cfg["keepalive_expiry"],
            ),
            timeout=httpx.Timeout(final_httpx_cfg["timeout"]),
        ) as client:
            data = await asyncio.gather(
                *[
                    asyncio.ensure_future(process_request(client, url, progress, task))
                    for url in urls
                ]
            )

        df = pd.DataFrame(data)

        if "season" in df:
            df["season"] = pd.to_numeric(df["season"], errors="coerce")
        if "episode" in df:
            df["episode"] = pd.to_numeric(df["episode"], errors="coerce")

    print("Finished scraping.")

    return df


def get_episode_urls(show: str, season: Optional[int] = None) -> List[Optional[str]]:
    """
    Scrape for list of episode URLs if this is a show

    Parameters
    ----------
    show: str
        The name of show
    season: str, default: None
        The season of the show to query from

    Returns
    -------
    List[str]
        The list of episode URLs

    """

    query_url = f"{MAIN_URL}/episode_scripts.php?tv-show={show}"

    _print_season_msg = ""
    if season is not None:
        query_url += f"&season={season:d}"
        _print_season_msg = f" (season {season:d})"

    print(f"Querying {show}{_print_season_msg}", end="")

    epls_soup = request(query_url, soup=True)
    assert isinstance(epls_soup, BeautifulSoup)
    ep_hrefs = [
        href if href.startswith(MAIN_URL) else f"{MAIN_URL}/{href}"
        for h in epls_soup.findAll("a", href=True)
        if "view_episode_scripts.php" in (href := h.get("href").strip())
    ]

    print(f"\t ... found {len(ep_hrefs)} links.")
    return ep_hrefs


def __fmt_term__(term: str, is_filename: bool = False) -> str:
    """Format a string to remove spaces and (if `is_filename=True`) also `(`, `)` and lower"""
    term = re.sub(r"\s+", "-", term.strip())
    if is_filename:
        term = term.replace("(", "").replace(")", "").lower()
    return term


def scrape(
    show: Union[None, List[str], str] = None,
    season: Optional[int] = None,
    movie: Union[None, List[str], str] = None,
    warning: bool = True,
    httpx_cfg: dict = dict(),
) -> pd.DataFrame:
    """
    Scrape scripts of show(s) and/or movie(s)

    Parameters
    ----------
    show: str, List[str] or None, default: None
        A single show or list of shows.
    season: int, default: None
        A season to query show from. If `show` is a list, will ignore.
    movie: str, List[str] or None, default: None
        A single movie or list of movies.
    warning: bool, default: True
        Will throw warnings
    httpx_cfg: dict, default: dict()
        Configuration for HTTPX. If any of the following is empty, will use `sf2.httpx_def_cfg`.
        Please refer to `httpx` documentation for the following:
        - `retries`
        - `max_keepalive_connections`
        - `max_connections`
        - `keepalive_expiry`
        - `timeout`

    Returns
    -------
    pd.DataFrame
        A pandas DataFrame with the results from scraping

    """

    assert not (
        show is None and movie is None
    ), "Both `show` and `movie` cannot be both `None`"

    urls = []

    if show is not None:
        if not isinstance(show, list):
            urls.extend(get_episode_urls(__fmt_term__(show), season=season))
        else:
            if len(show) == 1:
                urls.extend(get_episode_urls(__fmt_term__(show[0]), season=season))
            else:
                if season is not None and warning:
                    warnings.warn(
                        f"Ignoring `season={season}` when a list of shows is given"
                    )
                for s in set(show):
                    urls.extend(get_episode_urls(__fmt_term__(s)))

    if movie is not None:
        if not isinstance(movie, list):
            movie = [movie]
        urls.extend(
            [f"{MAIN_URL}/movie_script.php?movie={__fmt_term__(m)}" for m in set(movie)]
        )

    url_list: List[str] = list(set([_u for _u in urls if _u is not None]))
    assert len(url_list) > 0, "Empty list of URLs"
    df = asyncio.run(scrape_scripts(url_list, httpx_cfg=httpx_cfg))

    return df


def save_as(
    df: pd.DataFrame,
    file_path: str,
    file_format: str,
    obj_type: Optional[str] = None,
    obj_value: Optional[str] = None,
) -> None:
    """Save a single `DataFrame` (either just a single movie or show)"""

    file_format = file_format.lower()
    assert file_format in ["pkl", "csv", "json"]
    file_suffix = f"_scripts.{file_format}"

    if not file_path.endswith(file_suffix):
        file_path = file_path + file_suffix

    if file_format == "pkl":
        df.to_pickle(file_path)

    if file_format == "csv":
        df.to_csv(file_path, index=False)

    if file_format == "json":
        df.to_json(file_path, orient="records")

    if None not in [obj_type, obj_value]:
        print(f'+ {str(obj_type).upper()}: {obj_value} saved at "{file_path}"')
    else:
        print(f'+ Data saved at "{file_path}"')


def save(
    df: pd.DataFrame, dir_path: Optional[str] = None, file_format: str = "json"
) -> None:
    """
    Save the output `DataFrame` from requesting

    Parameters
    ----------
    df: pd.DataFrame
        The `DataFrame` from the processed requests
        Each show (with all seasons) and each movie will be saved in a separate file
    dir_path: str, default: None
        The directory path to which the data frames will be saved to.
        The default (`None`) will be the current directory
    file_format: str, default: `'json'`
        File format for each data frame.
        Only `'pkl'`, `'csv'`, `'json'` are accepted

    """

    if dir_path is None:
        dir_path = "."
    else:
        assert os.path.exists(
            dir_path
        ), f'The given directory "{dir_path}" does not exist'

    objects = []
    if "movie" in df:
        objects.extend([("movie", m) for m in df["movie"].dropna().unique()])
    if "show" in df:
        objects.extend([("show", s) for s in df["show"].dropna().unique()])

    for obj_type, obj_value in objects:
        file_name = __fmt_term__(obj_value, is_filename=True)
        df_sel = df.query(obj_type + " == @obj_value").reset_index(drop=True)

        if obj_type == "movie":
            assert len(df_sel) == 1

        file_path = os.path.join(dir_path, file_name)

        save_as(
            df_sel,
            file_path=file_path,
            file_format=file_format,
            obj_type=obj_type,
            obj_value=obj_value,
        )


def scrape_and_save(
    show: Union[None, List[str], str] = None,
    season: Optional[int] = None,
    movie: Union[None, List[str], str] = None,
    dir_path: Optional[str] = None,
    file_format: str = "json",
    warning: bool = False,
    httpx_cfg: dict = dict(),
) -> None:
    """
    Scrape scripts of show(s) and/or movie(s) and then save the output data frames
    for each show (with specified or all seasons) and each movie in a separate file

    Parameters
    ----------
    show: str, List[str] or None, default: None
        A single show or list of shows.
    season: int, default: None
        A season to query show from. If `show` is a list, will ignore.
    movie: str, List[str] or None, default: None
        A single movie or list of movies.
    dir_path: str, default: None
        The directory path to which the data frames will be saved to.
        The default (`None`) will be the current directory
    file_format: str, default: `'json'`
        File format for each data frame.
        Only `'pkl'`, `'csv'`, `'json'` are accepted
    warning: bool, default: True
        Will throw warnings
    httpx_cfg: dict, default: dict()
        Configuration for HTTPX. If any of the following is empty, will use `sf2.httpx_def_cfg`.
        Please refer to `httpx` documentation for the following:
        - `retries`
        - `max_keepalive_connections`
        - `max_connections`
        - `keepalive_expiry`
        - `timeout`

    """

    df = scrape(
        show=show, season=season, movie=movie, warning=warning, httpx_cfg=httpx_cfg
    )

    save(df, dir_path=dir_path, file_format=file_format)
