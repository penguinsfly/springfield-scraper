"""Springfield! Springfield! script scraper argument parser."""

import argparse
import pkgutil

import yaml

def_cfg = yaml.safe_load(pkgutil.get_data(__package__, "default_config.yaml"))  # type: ignore [arg-type]
httpx_def_cfg = def_cfg["httpx"]


def cli():
    """Command-line to parse arguments then run scraping, as well as saving output"""

    examples = """EXAMPLE:
        sf2 --show veep
        sf2 --show veep --season 2 --output-directory data --format csv
        sf2 --movie the-endless --format pkl
        sf2 --show veep stranger-things-2016 legion-2017 \\
            --movie the-endless something-in-the-dirt synchronic resolution \\
            --output-directory data --format json
    """

    parser = argparse.ArgumentParser(
        prog="sf2",
        description="Scraper for transcripts from <https://www.springfieldspringfield.co.uk>",
        epilog=examples,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    group_parsers = dict(
        main=parser.add_argument_group(
            title="INPUT & OUTPUT",
            description="Arguments used for querying on the website and for output",
        ),
        httpx=parser.add_argument_group(
            title="HTTPX CONFIGURATION",
            description="Arguments defining limits for httpx client configurations & requests",
        ),
    )

    group_parsers["main"].add_argument(
        "--show",
        type=str,
        nargs="*",
        help="TV show name from website. Can be a list.",
        default=None,
    )

    group_parsers["main"].add_argument(
        "--movie", type=str, nargs="*", help="Movie name from website", default=None
    )

    group_parsers["main"].add_argument(
        "--season",
        type=int,
        help="Season to query (only one). This will be ignored "
        "if only movies are queried, or multiple shows are queried. "
        "Default: scrape all seasons",
        default=None,
    )

    group_parsers["main"].add_argument(
        "-o",
        "-d",
        "--output-directory",
        dest="dir_path",
        type=str,
        help="Directory to save TV show and/or movie scripts to. "
        "Each show will be saved into one file, with all or requested seasons. "
        "Each movie will be saved into one file. "
        "Default: current directory",
        default=None,
    )

    group_parsers["main"].add_argument(
        "-f",
        "--format",
        dest="file_format",
        choices=["csv", "pkl", "json"],
        help="File format. Default: json",
        default="json",
    )

    group_parsers["main"].add_argument(
        "--warning", action="store_true", help="Throw warnings"
    )

    group_parsers["httpx"].add_argument(
        "-r",
        "--retries",
        type=int,
        help=f'Number of retries. Default: {httpx_def_cfg["retries"]}',
        default=httpx_def_cfg["retries"],
    )

    group_parsers["httpx"].add_argument(
        "-c",
        "--max-connections",
        "--max_connections",
        type=int,
        help="Max # concurrent connections to be established."
        f'Default: {httpx_def_cfg["max_connections"]}',
        default=httpx_def_cfg["max_connections"],
    )

    group_parsers["httpx"].add_argument(
        "-k",
        "--max-keepalive-connections",
        "--max_keepalive_connections",
        type=int,
        help="Allow the connection pool to maintain "
        "keep-alive connections below this point. "
        f'Default: {httpx_def_cfg["max_keepalive_connections"]}',
        default=httpx_def_cfg["max_keepalive_connections"],
    )

    group_parsers["httpx"].add_argument(
        "-e",
        "--keepalive-expiry",
        "--keepalive_expiry",
        type=int,
        help="Allow the connection pool to maintain "
        "keep-alive connections below this point. "
        f'Default: {httpx_def_cfg["keepalive_expiry"]}',
        default=httpx_def_cfg["keepalive_expiry"],
    )

    group_parsers["httpx"].add_argument(
        "-t",
        "--timeout",
        type=float,
        help="Timeout limit (seconds) for requests. "
        f'Default: {httpx_def_cfg["timeout"]}',
        default=httpx_def_cfg["timeout"],
    )

    args = parser.parse_args()

    assert (
        args.show is not None or args.movie is not None
    ), "Need to provide at least `--show` or `--movie`"

    group_args = dict()
    for grp_name, grp_parser in group_parsers.items():
        group_args[grp_name] = {
            a.dest: getattr(args, a.dest, None) for a in grp_parser._group_actions
        }

    run(group_args["main"], group_args["httpx"])


def run(main_args, httpx_cfg):
    """Run the program after parsing"""

    from . import sf2

    sf2.scrape_and_save(**main_args, httpx_cfg=httpx_cfg)
