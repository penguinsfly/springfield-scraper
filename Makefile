SHELL := /bin/bash
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

install_env: ## Install development environment
	python -m venv .venv
	source .venv/bin/activate
	pip install -e .[dev]

format: ## Lint format
	black src
	isort src

lint: ## Lint checks
	flake8 .
	black --check src
	isort --check src
	codespell
	mypy src
	pydocstyle

clean: ## Clean everything
	find . -type f -name *.pyc -delete
	find . -type d -name __pycache__ -delete
	find . -type d -name .mypy_cache -exec rm -rf {} \+
	find . -type d -name .egg-info -exec rm -rf {} \+
	[ ! -d build ] || rm -rf build
