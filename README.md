# Springfield! Springfield! script scraper

Scraper for **S**pring**f**ield! **S**pring**f**ield! TV show and movie transcripts from <https://www.springfieldspringfield.co.uk>.

## Installation

```bash
pip install git+https://codeberg.org/penguinsfly/springfield-scraper
```

## Usage

### CLI

```bash
usage: sf2 [-h] [--show [SHOW ...]] [--movie [MOVIE ...]] [--season SEASON]
           [-o DIR_PATH] [-f {csv,pkl,json}] [--warning] [-r RETRIES]
           [-c MAX_CONNECTIONS] [-k MAX_KEEPALIVE_CONNECTIONS]
           [-e KEEPALIVE_EXPIRY] [-t TIMEOUT]

Scraper for transcripts from <https://www.springfieldspringfield.co.uk>

options:
  -h, --help            show this help message and exit

INPUT & OUTPUT:
  Arguments used for querying on the website and for output

  --show [SHOW ...]     TV show name from website. Can be a list.
  --movie [MOVIE ...]   Movie name from website
  --season SEASON       Season to query (only one). This will be ignored if
                        only movies are queried, or multiple shows are
                        queried. Default: scrape all seasons
  -o DIR_PATH, -d DIR_PATH, --output-directory DIR_PATH
                        Directory to save TV show and/or movie scripts to.
                        Each show will be saved into one file, with all or
                        requested seasons. Each movie will be saved into one
                        file. Default: current directory
  -f {csv,pkl,json}, --format {csv,pkl,json}
                        File format. Default: json
  --warning             Throw warnings
```

For example

```bash
sf2 --show veep --season 2
```

<details><summary>HTTPx configurations</summary>

```bash
HTTPX CONFIGURATION:
  Arguments defining limits for httpx client configurations & requests

  -r RETRIES, --retries RETRIES
                        Number of retries. Default: 2
  -c MAX_CONNECTIONS, --max-connections MAX_CONNECTIONS, --max_connections MAX_CONNECTIONS
                        Max # concurrent connections to be
                        established.Default: 40
  -k MAX_KEEPALIVE_CONNECTIONS, --max-keepalive-connections MAX_KEEPALIVE_CONNECTIONS, --max_keepalive_connections MAX_KEEPALIVE_CONNECTIONS
                        Allow the connection pool to maintain keep-alive
                        connections below this point. Default: 20
  -e KEEPALIVE_EXPIRY, --keepalive-expiry KEEPALIVE_EXPIRY, --keepalive_expiry KEEPALIVE_EXPIRY
                        Allow the connection pool to maintain keep-alive
                        connections below this point. Default: 5
  -t TIMEOUT, --timeout TIMEOUT
                        Timeout limit (seconds) for requests. Default: 10
```

</details>

### Python

```python
from sf2 import sf2

sf2.scrape(
    show='veep',
    season=2,
)
```

## Disclaimer

This tool is originally designed for educational, research, archival and accessibility purposes.

Below is the disclaimer from Springfield! Springfield! website:

> **Disclaimer**
>
> This website is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and linking to `amazon.com` and `amazon.co.uk`.
>
> **Fair Use**
>
> Should content on this site be found to be copyrighted material, we are making such material available for research and educational purposes (including text mining, web mining and data mining), historical purposes and hearing-impaired individuals, without financial impact or harm to the brand or creator of the copyright owner. We believe this constitutes a ‘fair use’ of any such copyrighted material.
>
> This website does not offer downloading and/or streaming of tv show or movies or does it link to tv show or movie downloads.
>
> *https://www.springfieldspringfield.co.uk/disclaimer.php*

In addition, this tool does not collect personal or sensitive data about/of its user, and as far as I'm aware, the dependencies do not either. You should also visit the Springfield! Springfield! website [privacy notice](https://www.springfieldspringfield.co.uk/privacy.php) for more information as well.

If you spot any issue with this statement, the code of the tool itself, the dependencies or the Springfield! Springfield! website, please raise an issue.
